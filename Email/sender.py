#
# Python Version: 	3.8.1
# Language:       	English
# Platform:       	Win10
# Author:         	J.Tejada <jtejada@relevantradio.com>
# Project:			XDS Cleanup
#
from datetime import datetime
import win32com.client as comobj


class Email_Sender:
    DEFAULT_BODY = """\

    Cleaning Complete,


    Sincerely,
    ---Processing Robot---

    """

    def __init__(self, recipient_email):
        self.app = comobj.Dispatch('Outlook.Application')
        self.recipient_email = recipient_email
        self.subject = self.subject_line()
        self.body = self.DEFAULT_BODY
        self.mail_item = self.app.CreateItem(0)

    def subject_line(self):
        date_string = datetime.today().strftime('%m/%d/%Y')
        return f'Cleaning Complete {date_string}'

    def setup_email(self):
        self.mail_item.To = self.recipient_email
        self.mail_item.Subject = self.subject
        self.mail_item.Body = self.body

    def send(self):
        self.mail_item.Send()


def send_reply_email(sender):
    email = Email_Sender(sender)
    email.setup_email()
    email.send()
