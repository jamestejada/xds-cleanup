#
# Python Version: 	3.8.1
# Language:       	English
# Platform:       	Win10
# Author:         	J.Tejada <jtejada@relevantradio.com>
# Project:			XDS Cleanup
#
import pytz
from datetime import datetime, timedelta
from dateutil.parser import parse
from Email.email_base import Email_BASE
from Cleaner.settings import INPUT_DOC_PATH


class Get_Documents(Email_BASE):

    DOC_PATH = INPUT_DOC_PATH
    TARGET_ACCOUNT = 'jtejada@relevantradio.com'
    TIME_INTERVAL = timedelta(minutes=15)
    EXPIRATION_FILE = ['experation', 'expiration']
    ISCI_FILE = ['week']
    TRIGGER_STRING = 'TRIGGER_STRING'

    def __init__(self, session=None):
        # super methods
        self.get_session = super().get_session
        self.find_folders = super().find_folders

        self.storage_path = self.DOC_PATH
        self.session = session if session else self.get_session()
        self.inboxes = self.find_folders('Inbox', self.session)  # list
        self.archives = self.find_folders('Archive', self.session)

    # Main method
    def process_folders(self):
        """ Loops through all inboxes
        """
        for inbox in self.inboxes:
            if inbox.Parent.Name == self.TARGET_ACCOUNT:
                return self.find_recent_item(inbox)
        return None

    def archive_item(self, item):
        """ moves mail item to archive after downloading document.
        """
        # study archiver.py in Email Handler
        target_archive = self.get_target_archive()
        item.Move(target_archive)

    def get_target_archive(self):
        """ Returns target archive folder for processing account
        """
        for folder in self.archives:
            if folder.Parent.Name == self.TARGET_ACCOUNT:
                return folder

    def find_recent_item(self, folder):
        """ Finds an email that has recently been recieved with correct
        attachments and trigger string.
        """
        count = 0

        for item in reversed(folder.Items):
            if self.process_conditions(item):
                attachment_list = list(item.Attachments)
                for attachment in attachment_list:
                    file_name = attachment.FileName
                    new_file_name = self.file_name_check(file_name)
                    if new_file_name:
                        self.download_one(attachment, new_file_name)
                self.archive_item(item)
                return item.SenderEmailAddress
            count += 1
            if count > 200:
                return None
        # just in case
        return None

    def process_conditions(self, item):
        return (
            (len(item.Attachments) > 0)
            and self.check_for_trigger_string(str(item.Body))
            and self.check_recent_email(item.ReceivedTime)
        )

    def file_name_check(self, file_name: str) -> str:
        """ Checks file_name to see which file it is (expiration or isci)
        """
        for keyword in self.EXPIRATION_FILE:
            if keyword in file_name.lower():
                return 'EXPIRATION.xls'
        for keyword in self.ISCI_FILE:
            if keyword in file_name.lower():
                return 'ISCI.xls'
        return None

    def check_for_trigger_string(self, item_body: str):
        """ Checks that email contains trigger string
        """
        return (self.TRIGGER_STRING in item_body)

    def check_recent_email(self, outlook_date):
        """ checks that a mail item has been sent within RECENT_LIMIT
        """
        converted_ol_date = parse(str(outlook_date))
        pacific_tz = pytz.timezone("US/Pacific")
        adjusted_now = datetime.now(pacific_tz) - timedelta(hours=8)

        return ((adjusted_now - converted_ol_date) < self.TIME_INTERVAL)

    def download_one(self, attachment, out_file_name):
        """ Downloads one file
        """
        # Might throw exception
        destination_path = self.DOC_PATH.joinpath(out_file_name)
        print(f'{attachment.FileName} downloading to {destination_path.absolute()}')
        attachment.SaveAsFile(destination_path.absolute())


def main():
    getter = Get_Documents()
    return getter.process_folders()


if __name__ == '__main__':
    main()
