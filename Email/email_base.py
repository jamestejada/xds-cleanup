#
# Python Version: 	3.8.1
# Language:       	English
# Platform:       	Win10
# Author:         	J.Tejada <jtejada@relevantradio.com>
# Project:			Email Aggregator
#
import win32com.client as comobj


class Email_BASE:
    """ Collection of static methods for Outlook Email Classes
    """
    # def __init__(self, session=None):
    #   self.session = session if session else self.get_session()
    #   self.inboxes = self.find_folders('Inbox', self.session)     # list
    #   self.archive = self.find_folder('Archive', self.session)

    @staticmethod
    def get_session():
        """ creates a COM Object for Outlook and returns the session.
        """
        print('Loading Outlook...')
        outlook = comobj.Dispatch('Outlook.Application')
        return outlook.Session

    @staticmethod
    def find_folders(target_name, folder_obj) -> list:
        """ returns a list of folders titled {target_name}
        given the current outlook session.
        """
        print(f'Getting {target_name} Folders...')
        target_folders = []
        for folder in folder_obj.Folders:
            for subfolder in folder.Folders:
                if subfolder.Name == target_name:
                    target_folders.append(subfolder)
        return target_folders
