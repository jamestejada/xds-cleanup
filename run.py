#
# Python Version: 	3.8.1
# Language:       	English
# Platform:       	Win10
# Author:         	J.Tejada <jtejada@relevantradio.com>
# Project:			XDS Cleanup
#

# TO DO:
# - Handle errors caused by invalid user input for askopenfilename
# - Create a base class for backup and recheck
# DONE - allow function to "find" headers by incrementing header row
#       until there is no KeyError

import sys
import time
import logging
from Cleaner.XDS_backup import XDS_backup
from Cleaner.XDS_verify import XDS_verify
from Cleaner.XDS_delete import XDS_delete
from Cleaner.XDS_recheck import XDS_recheck
from Cleaner.settings import LOG_PATH
from Cleaner.settings import get_current_file
from Cleaner.settings import DEFAULT_EXP_FILE, DEFAULT_ISCI_FILE
from Cleaner.settings import SERVER_WAIT


def upload_only_check() -> bool:
    """ Gets passed arguments to determine execution path.
    """
    upload_only_arg = ['--upload-only', '-u', '--upload']
    argument_list = sys.argv[1:]
    for argument in argument_list:
        return (argument in upload_only_arg)


def get_logger():
    # initialize logging
    logging.basicConfig(
        filename=LOG_PATH,
        filemode='a',
        format='%(asctime)s %(name)s %(levelname)s %(message)s',
        datefmt='%H:%M:%S',
        level=logging.DEBUG
    )
    return logging.getLogger('Cleaner')


def server_run():
    """ runs program from email scanning server
    """
    Cleaner_log = get_logger()
    expiration_file = DEFAULT_EXP_FILE
    recheck_file = DEFAULT_ISCI_FILE
    killswitch = True

    logging.debug(f'Expiration File: {expiration_file.absolute()}')
    logging.debug(f'Recheck File: {recheck_file.absolute()}')
    Cleaner_log.info(f'killswitch set to: {killswitch}')

    XDS_backup(expiration_file)
    time.sleep(SERVER_WAIT)
    XDS_verify()
    time.sleep(SERVER_WAIT)
    XDS_delete(killswitch)
    time.sleep(SERVER_WAIT)
    XDS_recheck(recheck_file, upload_only=False)

    Cleaner_log.info('-------Cleaning Complete-------')


def main():

    Cleaner_log = get_logger()

    # upload_only gives the user the option to only upload files from the
    # full XDS archive based on a current isci report.
    # Having "if not upload_only" condition allows us to skip the other parts.
    upload_only = upload_only_check()

    if not upload_only:
        expiration_file = get_current_file('Please Choose ISCI Expiration Report')
        logging.debug(f'Expiration File: {expiration_file.absolute()}')

    recheck_file = get_current_file('Please Choose Latest ISCI Report')
    logging.debug(f'Recheck File: {recheck_file.absolute()}')

    if not upload_only:
        killswitch = (input("Type 'DELETE' to engage killswitch: ") == 'DELETE')

        Cleaner_log.debug(f'killswitch set to: {killswitch}')
        print(f'killswitch set to: {killswitch}')

        XDS_backup(expiration_file)
        XDS_verify()
        XDS_delete(killswitch)

    XDS_recheck(recheck_file, upload_only=upload_only)

    finished_message = 'Uploads Complete' if upload_only else 'Cleaning Complete'

    Cleaner_log.info(f'-------{finished_message}-------')
    print(f'-------{finished_message}-------')


if __name__ == '__main__':
    main()
