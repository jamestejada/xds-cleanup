@echo off

IF EXIST "%cd%\venv\" GOTO REQUIREMENTS

GOTO NOTFOUND

:REQUIREMENTS
python -m pip install --upgrade pip
pip install -r requirements.txt
GOTO EOF

:NOTFOUND
echo.
echo Virtual Environment not found. Please run env.bat first...
echo.
echo simply type 'env' into the command line
echo.

:EOF