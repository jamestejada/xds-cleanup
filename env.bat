@echo off

IF NOT EXIST "%cd%\venv\" GOTO CREATE

GOTO EOF

:CREATE
echo Creating Virtual Environment...
python -m venv venv
echo Created! Please run setup.bat
echo.
echo Simply type 'setup' into the command line
echo.

:EOF
"%cd%\venv\Scripts\activate.bat"