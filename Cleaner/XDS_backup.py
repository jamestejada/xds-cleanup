#
# Python Version: 	3.8.1
# Language:       	English
# Platform:       	Win10
# Author:         	J.Tejada <jtejada@relevantradio.com>
# Project:          XDS Cleanup
#

from datetime import datetime
from hashlib import md5
import logging
import pandas as pd
from Cleaner.settings import (
    XDS_ARCHIVE_PATH,
    XDS_BACKUP_PATH,
    XDS_connect
)

XDS_backup_logger = logging.getLogger('Cleaner.XDS_backup')


def get_excel_set(current_file):

    print('Reading File: ', current_file.resolve())
    for header_row in range(10):
        try:
            full_frame = pd.read_excel(current_file, header=header_row)
            no_null_frame = full_frame[['ISCI Code', 'End']].dropna()
            break
        except KeyError:
            continue

    filtered_frame = no_null_frame.loc[no_null_frame['End'] < datetime.today()]
    filtered_list = filtered_frame['ISCI Code'].tolist()
    excel_set = map(add_mp2, filtered_list)

    return set(excel_set)


def add_mp2(file_stem):
    return f'{file_stem}.mp2'


# get set from XDS FTP
def get_XDS_set(server):

    file_list = []
    server.retrlines('NLST', file_list.append)
    file_set = map(lowercase_ext, file_list)

    return set(file_set)


def lowercase_ext(file_name: str) -> str:
    return file_name.replace('.MP2', '.mp2')


def download_file(file_name, target_dir, server):

    full_path = target_dir.joinpath(file_name)

    with open(full_path, 'wb') as one_file:
        XDS_backup_logger.debug(
            f"{file_name} downloaded to {target_dir.absolute()}"
        )
        result = server.retrbinary(f"RETR {file_name}", one_file.write)
    return ('226' in result)


def get_hash(file_name, dir_path):
    with open(dir_path.joinpath(file_name), 'rb') as one_file:
        file_contents = one_file.read()
        return md5(file_contents).hexdigest()


def download_outdated_files(file_list, server):

    for file_name in file_list:

        time_out = 0

        while time_out < 5:

            print(f'{file_name}.', end='\r')
            result1 = download_file(file_name, XDS_ARCHIVE_PATH, server)
            print(f'{file_name}..  ', end='\r')
            result2 = download_file(file_name, XDS_BACKUP_PATH, server)

            print(f'{file_name}... ', end='\r')
            hash1 = get_hash(file_name, XDS_ARCHIVE_PATH)
            print(f'{file_name}....', end='\r\n')
            hash2 = get_hash(file_name, XDS_BACKUP_PATH)

            time_out += 1
            XDS_backup_logger.debug(f'Attempt #{time_out}')

            if (hash1 == hash2) and result1 and result2:
                break
            else:
                XDS_backup_logger.warning(
                    f'----------{file_name} FAILED----------'
                )

            XDS_backup_logger.debug(f'Archive Hash: {hash1}')
            XDS_backup_logger.debug(f'Backup Hash: {hash2}')
            XDS_backup_logger.debug(f'Archive Download Result: {result1}')
            XDS_backup_logger.debug(f'Backup Download Result: {result2}')


# Main Function
def XDS_backup(current_file):

    XDS_backup_logger.info('Entered XDS_backup module')

    server = XDS_connect()

    # find which files are outdated on server
    xl_set = get_excel_set(current_file)
    XDS_set = get_XDS_set(server)

    # list of files on server that are overdue
    outdated_list = list(xl_set.intersection(XDS_set))

    print(f'Number of Outdated Files: {len(outdated_list)}')
    XDS_backup_logger.info(f'Number of Outdated Files: {len(outdated_list)}')
    XDS_backup_logger.debug(f'List of Outdated Files: {outdated_list}')

    # download to two different folders.
    download_outdated_files(outdated_list, server)

    XDS_backup_logger.info('Exiting XDS_backup module')
