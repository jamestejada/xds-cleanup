#
# Python Version: 	3.8.1
# Language:       	English
# Platform:       	Win10
# Author:         	J.Tejada <jtejada@relevantradio.com>
# Project:			XDS Cleanup
#
import os
import logging
from Cleaner.settings import XDS_ARCHIVE_PATH
from Cleaner.settings import XDS_connect

XDS_delete_logger = logging.getLogger('Cleaner.XDS_delete')


def XDS_delete(killswitch: bool):

    if killswitch:
        XDS_delete_logger.warning(
            '------KILLSWITCH ENGAGED------'
        )

    server = XDS_connect()
    file_list = os.listdir(XDS_ARCHIVE_PATH)

    for each_file in file_list:

        if killswitch:
            XDS_delete_logger.debug(f'Sending: Delete {each_file}')
            response = server.delete(each_file)
        else:
            XDS_delete_logger.debug(f'Sending: STAT {each_file}')
            response = server.sendcmd(f'STAT {each_file}')

        XDS_delete_logger.debug(f'Response: {response}')
        print(response)
