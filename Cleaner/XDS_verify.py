#
# Python Version: 	3.8.1
# Language:       	English
# Platform:       	Win10
# Author:         	J.Tejada <jtejada@relevantradio.com>
# Project:			XDS Cleanup
#

import os
import shutil
import logging
from Cleaner.XDS_backup import get_hash
from Cleaner.settings import (
    XDS_ARCHIVE_PATH,
    XDS_BACKUP_PATH,
    XDS_ERROR_PATH
)

XDS_verify_logger = logging.getLogger('Cleaner.XDS_verify')


def XDS_verify():

    file_list = os.listdir(XDS_ARCHIVE_PATH)

    for each_file in file_list:
        hash1 = get_hash(each_file, XDS_ARCHIVE_PATH)
        hash2 = get_hash(each_file, XDS_BACKUP_PATH)

        # if hashes do not match, move to error folder.
        if hash1 != hash2:
            XDS_verify_logger.warning(
                f'--------{each_file} NOT VERIFIED--------'
            )
            XDS_verify_logger.warning(f'moving to {XDS_ERROR_PATH}')

            shutil.move(
                str(XDS_ARCHIVE_PATH.joinpath(str(each_file)).resolve()),
                str(XDS_ERROR_PATH.joinpath(str(each_file)).resolve()),
            )
        else:
            print(f"{each_file} verified")
            XDS_verify_logger.debug(f"{each_file} verified")

    #       should I have it also check the length
    #       of the file against the expected length
    #       from the report?
