#
# Python Version: 	3.8.1
# Language:       	English
# Platform:       	Win10
# Author:         	J.Tejada <jtejada@relevantradio.com>
# Project:			XDS Cleanup
#
from datetime import datetime
from pathlib import Path
from ftplib import FTP
from tkinter import Tk
from tkinter.filedialog import askopenfilename
import logging
import os

# Start Logging
logging.getLogger('Cleaner.settings')

# Calculate datetime for folder path
folder_date = datetime.today().strftime("%Y%m%d_%H%M%S")

# Server Wait Time (to prevent ftp server errors)
SERVER_WAIT = 30

# Set Paths
XDS_FULL_ARCHIVE = Path("U:\\XDS Archive")  # Migrate to new place.
XDS_ARCHIVE_PATH = Path.cwd().joinpath("downloads", folder_date, "XDS Archive")
XDS_BACKUP_PATH = Path.cwd().joinpath("downloads", folder_date, "XDS Backup")
XDS_ERROR_PATH = Path.cwd().joinpath("downloads", folder_date, "ERROR")
INPUT_DOC_PATH = Path.cwd().joinpath('documents')
LOG_DIR = Path.cwd().joinpath('logs')

DEFAULT_EXP_FILE = INPUT_DOC_PATH.joinpath('EXPIRATION.xls')
DEFAULT_ISCI_FILE = INPUT_DOC_PATH.joinpath('ISCI.xls')

# Create Directories
XDS_ARCHIVE_PATH.mkdir(parents=True, exist_ok=True)
XDS_BACKUP_PATH.mkdir(parents=True, exist_ok=True)
XDS_ERROR_PATH.mkdir(parents=True, exist_ok=True)
INPUT_DOC_PATH.mkdir(parents=True, exist_ok=True)
LOG_DIR.mkdir(parents=True, exist_ok=True)

LOG_PATH = LOG_DIR.joinpath(f'{folder_date}.log')


# Connect to XDS
def XDS_connect(n=1):
    """ This function instantiates an FTP object and logs into the XDS
        FTP. If the login fails it calls itself recursively.

        Returns: FTP object
    """
    if n > 3:
        return

    print('------logging in to XDS------')
    XDS_IP = os.getenv('XDS_IP')
    XDS_USER = os.getenv('XDS_USER')
    XDS_PASSWORD = os.getenv('XDS_PASSWORD')

    XDSserver = FTP(XDS_IP)
    result = XDSserver.login(XDS_USER, XDS_PASSWORD)

    logging.debug(f'Attempt {n} - Logging in to XDS - result: {result}')

    if (result == '230 User logged in.'):
        print('------Connected to XDS------')
        return XDSserver
    else:
        XDS_connect(n+1)
#   End XDS Connect


def get_current_file(window_title: str):
    Tk().withdraw()
    return Path(askopenfilename(
        filetypes=[
            ('Excel Files', '*.xlsx'),
            ('Excel Files', '*.xls')
        ],
        title=window_title
        ))
