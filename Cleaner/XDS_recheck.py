#
# Python Version: 	3.8.1
# Language:       	English
# Platform:       	Win10
# Author:         	J.Tejada <jtejada@relevantradio.com>
# Project:			XDS Cleanup
#
import os
import shutil
import time
import logging
import ftplib
import pandas as pd
from Cleaner.settings import XDS_ARCHIVE_PATH, XDS_FULL_ARCHIVE
from Cleaner.settings import XDS_connect
from Cleaner.settings import SERVER_WAIT

XDS_recheck_logger = logging.getLogger('Cleaner.XDS_recheck')


def get_xl_set(current_file):

    print('Reading File: ', current_file.resolve())
    for header_row in range(10):
        try:
            full_frame = pd.read_excel(current_file, header=header_row)
            filtered_frame = full_frame[['ISCI Code']].dropna()
            break
        except KeyError:
            continue
    processed_list = map(
        lambda isci: f'{isci}.mp2',
        filtered_frame['ISCI Code'].tolist()
    )

    return set(processed_list)


# get XDS set
def get_XDS_set(server) -> set:
    file_list = []
    server.retrlines('NLST', file_list.append)
    file_set = map(lowercase_ext, file_list)

    return set(file_set)


def lowercase_ext(file_name: str) -> str:
    return file_name.replace('.MP2', '.mp2')


# upload files
def upload_XDS_files(file_list: list, server, directory=XDS_ARCHIVE_PATH) -> str:

    print(f'Uploading outstanding files from {directory.absolute()}')

    if not directory.exists():
        return f'{directory.absolute()} does not exist.'

    result = 'Nothing Uploaded...'

    for each_file in file_list:
        fullPath = directory.joinpath(each_file)

        if fullPath.exists():
            with open(fullPath, 'rb') as one_file:
                print(f'Uploading: {one_file.name}')
                success = False
                while not success:
                    try:
                        result = server.storbinary(f"STOR {each_file}", one_file)
                        success = True
                    except ftplib.error_perm as e:
                        print(e)
                        time.sleep(SERVER_WAIT)

                if '226' in result:
                    XDS_recheck_logger.debug(f'{one_file.name} Upload Result: {result}')
                else:
                    XDS_recheck_logger.warning(f'{one_file.name} Upload Result: {result}')
    return result


def move_to_full_archive():
    for item in os.listdir(XDS_ARCHIVE_PATH):
        shutil.copy(
            str(XDS_ARCHIVE_PATH.joinpath(item).resolve()),
            str(XDS_FULL_ARCHIVE.joinpath(item).resolve())
        )


# --------main-----------
def XDS_recheck(current_isci, upload_only=False):
    # get set from excel file
    xl_set = get_xl_set(current_isci)

    XDS_recheck_logger.info(f'ISCI Report: {current_isci}')
    XDS_recheck_logger.info(f'Excel Set Length: {len(xl_set)}')

    # get XDS set
    server = XDS_connect()
    XDS_set = get_XDS_set(server)

    XDS_recheck_logger.info(f'XDS Set Length: {len(XDS_set)}')

    # compare sets
    missing_list = list(xl_set.difference(XDS_set))

    XDS_recheck_logger.info(
        f'Outstanding Files List Length: {len(missing_list)}'
    )
    XDS_recheck_logger.debug(f'Outstanding Files List Length: {missing_list}')

    # upload files
    if not missing_list:
        print('No Outstanding Files')
    else:
        print(upload_XDS_files(missing_list, server, directory=XDS_FULL_ARCHIVE))

        if not upload_only:
            time.sleep(SERVER_WAIT)
            print(upload_XDS_files(missing_list, server, directory=XDS_ARCHIVE_PATH))

    print('Copying files to Full Archive')
    move_to_full_archive()
