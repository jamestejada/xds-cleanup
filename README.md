# XDS Cleanup

## Purpose
The purpose of this program is to clean up outdated files from the XDS FTP server in order to save bandwidth and reduce errors with XDS Satellite delivery system. 

![XDS Cleanup in Action!](https://i1.lensdump.com/i/jUOCGK.gif)

## Requirements
- Python 3.8 ([Download from python.org](https://www.python.org/ftp/python/3.8.5/python-3.8.5-amd64.exe))
- Program dependencies: See requirements.txt

## Setup

1. Install Python 3.8 ([Download from python.org](https://www.python.org/ftp/python/3.8.5/python-3.8.5-amd64.exe))
2. Set XDS FTP login credentials in your environment variables (for more help see [this video](https://youtu.be/IolxqkL7cD8?t=78) or ask your local IT person). You should be able to get these credentials from IT or Engineering. Make sure to include the underscore in your environment variable names.
    - XDS_IP
    - XDS_USER
    - XDS_PASSWORD
3. Run env.bat from the command line in the project folder
    ``` 
    > env
    ```
    This will create a virtual environment and enter it. A virtual environment basically keeps the parameters of the program consistent so that it will keep running predictably.<br/><br/>
    Here is an example of what the command line should look like after you run env.bat:
    ```
    (venv) C:\Users\jtejada\Desktop\Repos\XDScleanup>
    ```

4. Run setup.bat
    ```
    > setup
    ```
    This will install all the dependencies for this program.

5. Change the XDS_FULL_ARCHIVE variable in Cleaner.settings to the absolute path to the main XDS archive.

## How to Use This Program

### Re-Uploading Old Files (Upload Only Option)

1. Get the latest ISCI report. These are Excel files generated from Counterpoint.

2. Make sure you are on the command line in the project directory and run env.bat
    ```
    > env
    ```
    and your command prompt should look something like this:
    ```
    (venv) C:\Users\jtejada\Desktop\Repos\XDScleanup>
    ```

3. Run run.py with an upload only flag (`-u`,`--upload`, or `--upload-only`). This is what starts the program
    ```
    > run.py -u
    ```
    ```
    > run.py --upload
    ```
    ```
    > run.py --upload-only
    ```

4. You will be prompted to choose the latest ISCI report.

5. The program will run and should upload any older files that need re-uploading.


### Cleaning the XDS FTP

1. Get the latest Expiration report and the latest ISCI report. These are Excel files generated from Counterpoint. 

2. Make sure you are on the command line in the project directory and run env.bat
    ```
    > env
    ```
    and your command prompt should look something like this:
    ```
    (venv) C:\Users\jtejada\Desktop\Repos\XDScleanup>
    ```

3. Run run.py. This is what starts the program
    ```
    > run.py
    ```
    
4. You will be prompted to choose:
    - The latest Expiration Spreadsheet
    - The latest ISCI Report

5. You will then be prompted from the command line:
    ```
    Type 'DELETE' to engage killswitch: 
    ```
    - If you are ready to delete files from the XDS FTP type `DELETE` in all caps at the prompt.
    - If you are just testing hit return or enter another set of characters.

6. The program will run and show which files are downloading. After each file is a set of four dots which show which step of the process is being run.
    <br/><br/>SAMPLE OUTPUT:
    ```
    TAGN-073120-30-A....
    TDH-051820-60-A..
    ```

    1. Download file to XDS Archive Folder
    2. Download same file to XDS Backup Folder
    3. Hash each file
    4. Verify that the hashes match. 

    Hashing the files ensures the integrity of what has been downloaded so that we are sure a safe backup has been downloaded.

7. The program will again verify the files to ensure their integrity
    <br/><br/>SAMPLE OUTPUT:
    ``` 
    ABW-070620-60-A.mp2 verified
    AHA-051820-30-A.mp2 verified
    CCF-110419-60-A.mp2 verified
    ```

8. After verification the program will send delete commands to the server if you have engaged the killswitch.
    <br/><br/>SAMPLE OUTPUT:
    ``` 
    250 DELE command successful.
    250 DELE command successful.
    250 DELE command successful.
    ```

9. The program then uses the latest ISCI report to re-upload any files that have been recently restarted (This allows you to use older Expiration Reports if needed).

10. After running, transfer files from the "XDS Archive" for the day to the main XDS Archive.