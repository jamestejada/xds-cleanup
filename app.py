from fastapi import FastAPI
from run import server_run
from Email.document_get import main as get_documents
from Email.sender import send_reply_email

app = FastAPI()


@app.get('/')
async def home():
    sender = get_documents()
    if sender:
        server_run()
        send_reply_email(sender)
        return {
            'success': True,
            'message': 'cleaning complete'
        }

    return {
        'success': True,
        'message': 'no new documents found'
    }
